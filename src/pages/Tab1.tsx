import { IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonFab, IonFabButton, IonIcon, IonGrid, IonRow, IonCol, IonImg, IonActionSheet, IonText, } from '@ionic/react';
import React, { useState } from 'react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import { camera, trash, close } from 'ionicons/icons';
import { usePhotoGallery } from '../cameraStuff';
import { Photo } from '@capacitor/camera';
import { CameraPreview } from '@capacitor-community/camera-preview';

const Tab1: React.FC = () => {

  const {takePhoto, photo, faces} = usePhotoGallery()

  //TODO: Share feature
  //TODO: Deploy to phones
  //TODO: Display picture taken along with boxes (save with boxes y/n possibly?)
  //TODO: Camera with overlay of number of heads? https://youtu.be/JA8k738i9jQ

  return (
    <IonPage className="centered">
      <IonContent fullscreen>
          <IonText className="numFaces poiple">HEADS: {faces}</IonText>
          <IonFab color= "--ion-color-tertiary-shade" vertical="bottom" horizontal="center" slot="fixed">
            <IonFabButton onClick={takePhoto}>
              <IonIcon icon={camera}></IonIcon>
            </IonFabButton>
          </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
