import {
    Camera,
    CameraResultType,
    CameraSource,
    Photo} from '@capacitor/camera';
import {
    FaceClient} from "@azure/cognitiveservices-face";
import {
    CognitiveServicesCredentials
} from "@azure/ms-rest-azure-js";
import React, { useEffect } from 'react';
import { CameraPreviewOptions } from "@capacitor-community/camera-preview"

//https://ionicframework.com/docs/react/your-first-app/taking-photos
//https://reactjs.org/docs/hooks-reference.html#usestate
//https://www.pluralsight.com/guides/executing-promises-in-a-react-component
//https://reactjs.org/docs/hooks-faq.html#is-it-safe-to-omit-functions-from-the-list-of-dependencies
//https://github.com/ionic-team/tutorial-photo-gallery-react/blob/main/src/

/* NEVER SHARE THESE!!! */
const key = "e9969a0bd0b544efbd1dc9147a63cc36";
const endpoint = "https://head-count.cognitiveservices.azure.com/";

export function usePhotoGallery() {

    const [photo, setPhoto] = React.useState<Photo>();
    const [faces, setFaces] = React.useState(0);

    //https://stackoverflow.com/questions/53253940/make-react-useeffect-hook-not-run-on-initial-render

    useEffect(() => {
        if(photo !== undefined) {
            const blob = async() => await convertBase64toBlob(photo!.base64String)
            .then(async result => {
                await findFace(result)
                    .then(result => {
                        const numFaces = result.length
                        setFaces(numFaces);
                    })
            });
            blob().catch(console.error);
        }
    }, [photo])

    const takePhoto = async() => {
        const cameraPhoto = await Camera.getPhoto({
            quality: 100,
            source: CameraSource.Camera,
            resultType: CameraResultType.Base64
        }).catch(Error)
        setPhoto(cameraPhoto as Photo);
    }

    const findFace = async(blob: Blob) => {
        const faceKey = process.env["faceKey"] || key;
        const faceEndPoint = process.env["faceEndPoint"] || endpoint;
        const cognitiveServiceCredentials = new CognitiveServicesCredentials(faceKey);
        const client = new FaceClient(cognitiveServiceCredentials, faceEndPoint);
        const options = {
            returnFaceLandmarks: true
        };

        //https://stackoverflow.com/questions/66605033/pass-image-bitmap-to-azure-face-sdk-detectwithstream
        return await client.face.detectWithStream(blob);
        
    }

    return {takePhoto, photo, faces}

}

    //https://ionicframework.com/blog/converting-a-base64-string-to-a-blob-in-javascript/
    export async function convertBase64toBlob(base64Data?: string) {
        try {
            const base64Response = await fetch(`data:image/jpeg;base64,${base64Data}`);
            const blob = await base64Response.blob();
            return blob
        } catch (error) {
            throw(error)
        }
    }